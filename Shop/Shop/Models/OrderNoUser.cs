﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Models {
    public class OrderNoUser : Order {
        #region fields

        private string _street, _city, _firstname, _lastname, _email;
        private int _houseNumber, _postalCode;

        #endregion

        #region properties

        public string Firstname {
            get => _firstname;
            set => _firstname = value;
        }

        public string Lastname {
            get => _lastname;
            set => _lastname = value;
        }

        public string Email {
            get => _email;
            set => _email = value;
        }

        public string Street {
            get => _street;
            set => _street = !string.IsNullOrEmpty(value) ? value : "N/A";
        }

        public string City {
            get => _city;
            set => _city = !string.IsNullOrEmpty(value) ? value : "N/A";
        }

        public int HouseNumber {
            get => _houseNumber;
            set {
                if (value >= 0) {
                    _houseNumber = value;
                }
                else {
                    throw new ArgumentOutOfRangeException("House number must be greater than 0!");
                }
            }
        }

        public int PostalCode {
            get => _postalCode;
            set {
                if (value >= 0) {
                    _postalCode = value;
                }
                else {
                    throw new ArgumentOutOfRangeException("Postal code must be greater than 0!");
                }
            }
        }

        #endregion

        public OrderNoUser() : this(-1, DateTime.MinValue, null, null, null, null, null, null, -1, -1) { }

        public OrderNoUser(int id, DateTime orderDate, Dictionary<Article, int> articles, string street, string city,
            string firstname, string lastname,
            string email, int houseNumber, int postalCode) : base(id, orderDate, articles) {
            _street = street;
            _city = city;
            _firstname = firstname;
            _lastname = lastname;
            _email = email;
            _houseNumber = houseNumber;
            _postalCode = postalCode;
        }
    }
}