﻿using System;
using System.Web;
using System.Web.SessionState;
using System.Xml;

namespace Shop.Models {
    [Serializable]
    public enum Lang {
        en, de
    }

    public static class LanguageSupport {
        private static HttpServerUtility _server = HttpContext.Current.Server;
        private static Lang _currLang = Lang.en;

        public static Lang CurrentLanguage {
            get {
                return _currLang;
            }
            set { _currLang = value; }
        }

        public static string CurrentLanuageShortString {
            get {
                return Enum.GetName(typeof(Lang), _currLang).ToLower();
            }
        }

        public static string CurrentLanguageLongString {
            get {
                switch (_currLang) {
                    case Lang.en:
                        return GetText("lang_en");
                    case Lang.de:
                        return GetText("lang_de");
                }

                return GetText("lang_en");
            }
        }

        public static string GetText(string elementName) {

            XmlDocument doc = new XmlDocument();

            switch (_currLang) {
                case Lang.de:
                    doc.Load(_server.MapPath("~/App_Data/texts_de.xml"));
                    break;
                default:
                    doc.Load(_server.MapPath("~/App_Data/texts_en.xml"));
                    break;
            }

            XmlNode element = doc.SelectSingleNode("/texts/" + elementName);

            if (element != null) {
                return element.InnerText;
            }
            else {
                throw new ArgumentOutOfRangeException("The given element name does not exist!");
            }
        }
    }
}