﻿using System;

namespace Shop.Models {
    public class Address {
        #region properties

        public int Id { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public int HouseNumber { get; set; }
        public int PostalCode { get; set; }

        #endregion

        #region ctors

        public Address() : this(null, null, Int32.MinValue, Int32.MinValue, Int32.MinValue) { }

        public Address(string street, string city, int houseNumber, int postalCode, int id) {
            Street = street;
            City = city;
            HouseNumber = houseNumber;
            PostalCode = postalCode;
            Id = id;
        }

        #endregion
    }
}