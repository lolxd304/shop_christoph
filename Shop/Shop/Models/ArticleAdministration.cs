﻿using System.Collections.Generic;

namespace Shop.Models {
    public class ArticleAdministration {
        public int Id { get; set; }
        public string Name { get; set; }
        public string En { get; set; }
        public string De { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }
        public int Category { get; set; }

        public ArticleAdministration() : this(-1, null, null, null, -1.0m, null, -1) { }

        public ArticleAdministration(int id, string name, string descriptionEn, string descriptionDe, decimal price,
            string image, int category) {

            Id = id;
            Name = name;
            En = descriptionEn;
            De = descriptionDe;
            Price = price;
            Image = image;
            Category = category;
        }
    }
}