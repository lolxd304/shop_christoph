﻿namespace Shop.Models
{
    public class Category
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public Category():this(0, "") { }
        public Category(int id, string title) {
            Id = id;
            Title = title;
        }
    }
}