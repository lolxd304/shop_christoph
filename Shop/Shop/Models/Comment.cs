﻿using System;

namespace Shop.Models {
    public class Comment {
        public string Text { get; set; }
        public int Rating { get; set; }
        public User User { get; set; }

        public Comment() : this(null, Int32.MinValue, null) { }
        public Comment(string text, int rating, User user) {
            Text = text;
            Rating = rating;
            User = user;
        }
    }
}