﻿using System.Collections.Generic;

namespace Shop.Models {
    public enum UserRole {
        NoUser, RegisteredUser, Staff, Admin
    }

    public class User {
        #region properties

        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public UserRole Role { get; set; }
        public List<Address> Addresses { get; set; }

        #endregion

        #region ctors

        public User() : this(0, "", "", "", "", "", null) { }
        public User(int id, string username, string password, string email, string firstname, string lastname, List<Address> addresses) {
            Id = id;
            Username = username;
            Password = password;
            Email = email;
            Firstname = firstname;
            Lastname = lastname;
            Addresses = addresses;
        }

        #endregion
    }
}