﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Models
{
    public class Message
    {
        public string Headline { get; set; }
        public string Msg { get; set; }
        public string Solution { get; set; }
    }
}