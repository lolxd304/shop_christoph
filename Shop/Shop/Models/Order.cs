﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.Models
{
    public class Order
    {
        public int Id { get; set; }
        public DateTime OrderDate { get; set; }
        public Dictionary<Article, int> Articles { get; set; }

        public Order() :this(-1, DateTime.MinValue, null) { }
        public Order(int id, DateTime orderDate, Dictionary<Article, int> articles) {
            Id = id;
            OrderDate = orderDate;
            Articles = articles;
        }
    }
}