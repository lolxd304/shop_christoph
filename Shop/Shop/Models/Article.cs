﻿using System.Collections.Generic;

namespace Shop.Models {
    public class Article {
        #region fields

        private int _id;
        private string _name, _description;
        private decimal _price;
        private double _rating;

        #endregion

        #region properties

        public int Id {
            get => _id;
        }

        public string Name {
            get => _name;
            set => _name = value;
        }

        public string Description {
            get => _description;
            set => _description = value;
        }

        public decimal Price {
            get => _price;
        }

        public double Rating {
            get => _rating;
        }

        public string Image { get; set; }

        public List<Comment> Comments { get; set; }

        #endregion

        #region ctors

        public Article() : this(0, "N/A", "N/A", 0.0m, 0.0, null, new List<Comment>()) { }

        public Article(int id, string name, string description, decimal price, double rating, string image, List<Comment> comments) {
            _id = id;
            _name = name;
            _description = description;
            _price = price;
            _rating = rating;
            Image = image;
            Comments = comments;
        }

        #endregion

        #region methods

        public override string ToString() {
            return _name;
        }

        #endregion
    }
}