﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Shop.Models {
    public class Login {
        #region properties

        public string Username { get; set; }
        public string Password { get; set; }

        #endregion
    }
}