﻿using System;
using System.Collections.Generic;

namespace Shop.Models.Repositories {
    public interface IArticleRepository : IDisposable {
        Article GetArticle(int id);
        List<Article> GetAllArticles();
        List<Article> GetAllArticles(int limit, int offset);
        List<Article> GetAllArticlesFromCategory(int category);
        List<ArticleAdministration> GetAllAdministrationArticles();
        ArticleAdministration GetArticleAdministration(int id);
        List<Article> SearchArticles(string search);
        List<Article> SearchArticles(string search, int category);
        bool DeleteArticle(int id);
        bool CreateArticle(ArticleAdministration toBeCreated);
        bool UpdateArticle(ArticleAdministration newArticle);
        bool AddComment(int artId, Comment comment);
        List<Category> GetCategories();
        Category GetCategory(int id);
        bool OrderArticle(Order order, int? userId, int? addressId);
    }
}
