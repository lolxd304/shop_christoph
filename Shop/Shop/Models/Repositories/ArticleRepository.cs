﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using System.Web.Mvc;
using MySql.Data.MySqlClient;

namespace Shop.Models.Repositories {
    public class ArticleRepository : IArticleRepository, IMysqlConnect {
        #region fields

        private string _connectionString = ConfigurationManager.ConnectionStrings["defaultDB"].ConnectionString;
        private MySqlConnection _conn;

        #endregion

        #region ctors

        public ArticleRepository() {
            Open();
        }

        #endregion

        #region methods

        public void Open() {
            if (_conn == null) {
                _conn = new MySqlConnection(_connectionString);
            }

            if (_conn.State == ConnectionState.Open) {
                return;
            }

            try {
                _conn.Open();
            }
            catch (MySqlException ex) {
                throw;
            }
        }

        public void Close() {
            if (_conn != null && _conn.State == ConnectionState.Open) {
                _conn.Close();
                _conn = null;
            }
        }

        public void Dispose() {
            Close();
        }

        public Article GetArticle(int id) {
            string cmdText = $@"
                    SELECT a.id, a.name, d.{LanguageSupport.CurrentLanuageShortString} AS description, a.price, avg(c.rating) AS rating, a.img
                    FROM articles a 
                        INNER JOIN descriptions d ON a.id = d.artId 
                        LEFT JOIN comments c ON a.id = c.artId 
                    WHERE a.id = @id
                    GROUP BY a.id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("id", id);

            Article ret = null;

            try {
                using (MySqlDataReader reader = cmd.ExecuteReader()) {
                    if (reader.HasRows) {
                        reader.Read();
                        ret = RowToArticle(reader);
                    }
                }

                ret = AddCommentsToArticle(ret);
            }
            catch (MySqlException ex) {
                throw;
            }

            return ret;
        }

        public List<Article> GetAllArticles() {
            string cmdText = $@"
                    SELECT a.id, a.name, d.{LanguageSupport.CurrentLanuageShortString} AS description, a.price, avg(c.rating) AS rating, a.img
                    FROM articles a
                        INNER JOIN descriptions d ON a.id = d.artId
                        LEFT JOIN comments c ON a.id = c.artId
                    GROUP BY a.id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;

            List<Article> rets = null;

            try {
                using (MySqlDataReader reader = cmd.ExecuteReader()) {
                    if (reader.HasRows) {
                        rets = new List<Article>();
                        while (reader.Read()) {
                            rets.Add(RowToArticle(reader));
                        }
                    }
                }

                rets?.ForEach(a => a = AddCommentsToArticle(a));
            }
            catch (MySqlException ex) {
                throw;
            }

            return rets;
        }

        public List<Article> GetAllArticles(int limit, int offset) {
            string cmdText = $@"
                    SELECT a.id, a.name, d.{LanguageSupport.CurrentLanuageShortString} AS description, a.price, avg(c.rating) AS rating, a.img
                    FROM articles a
                        INNER JOIN descriptions d ON a.id = d.artId
                        LEFT JOIN comments c ON a.id = c.artId
                    GROUP BY a.id
                    LIMIT @limit";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("limit", limit);

            List<Article> rets = null;

            try {
                using (MySqlDataReader reader = cmd.ExecuteReader()) {
                    if (reader.HasRows) {
                        rets = new List<Article>();
                        while (reader.Read()) {
                            rets.Add(RowToArticle(reader));
                        }
                    }
                }

                rets?.ForEach(a => a = AddCommentsToArticle(a));
            }
            catch (MySqlException ex) {
                throw;
            }

            return rets;
        }

        public List<Article> GetAllArticlesFromCategory(int category) {
            string cmdText = $@"
                    SELECT a.id, a.name, d.{LanguageSupport.CurrentLanuageShortString} AS description, a.price, avg(c.rating) AS rating, a.img
                    FROM articles a
                        INNER JOIN descriptions d ON a.id = d.artId
                        LEFT JOIN comments c ON a.id = c.artId
                    WHERE a.category = @category
                    GROUP BY a.id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("category", category);

            List<Article> rets = null;

            try {
                using (MySqlDataReader reader = cmd.ExecuteReader()) {
                    if (reader.HasRows) {
                        rets = new List<Article>();
                        while (reader.Read()) {
                            rets.Add(RowToArticle(reader));
                        }
                    }
                }

                rets?.ForEach(a => a = AddCommentsToArticle(a));
            }
            catch (MySqlException ex) {
                throw;
            }

            return rets;
        }

        public List<Article> SearchArticles(string search) {
            string cmdText = $@"
                    SELECT a.id, a.name, d.{LanguageSupport.CurrentLanuageShortString} AS description, a.price, avg(c.rating) AS rating, a.img 
                    FROM articles a 
                        INNER JOIN descriptions d ON a.id = d.artId 
                        LEFT JOIN comments c ON a.id = c.artId 
                    WHERE LOWER(a.name) LIKE CONCAT('%', @search, '%')
                    GROUP BY a.id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("search", search.ToLower());

            List<Article> rets = null;

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                if (reader.HasRows) {
                    rets = new List<Article>();
                    while (reader.Read()) {
                        rets.Add(RowToArticle(reader));
                    }
                }
            }

            rets?.ForEach(a => a = AddCommentsToArticle(a));

            return rets;
        }

        public List<Article> SearchArticles(string search, int category) {
            string cmdText = $@"
                    SELECT a.id, a.name, d.{LanguageSupport.CurrentLanuageShortString} AS description, a.price, avg(c.rating) AS rating, a.img
                    FROM articles a 
                        INNER JOIN descriptions d ON a.id = d.artId 
                        LEFT JOIN comments c ON a.id = c.artId 
                    WHERE LOWER(a.name) LIKE CONCAT('%', @search, '%') AND a.category = @cat
                    GROUP BY a.id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("search", search.ToLower());
            cmd.Parameters.AddWithValue("cat", category);

            List<Article> rets = null;

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                if (reader.HasRows) {
                    rets = new List<Article>();
                    while (reader.Read()) {
                        rets.Add(RowToArticle(reader));
                    }
                }
            }

            rets?.ForEach(a => a = AddCommentsToArticle(a));

            return rets;

            return null;
        }

        public bool DeleteArticle(int id) {
            string cmdText = @"
                    DELETE
                    FROM articles
                    WHERE id = @id;";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("id", id);

            return cmd.ExecuteNonQuery() == 1;
        }

        public bool CreateArticle(ArticleAdministration toBeCreated) {
            string cmdInsArticle = $@"
                    INSERT INTO articles
                    VALUES (null, @name, @price, @category, {(toBeCreated.Image == null ? "DEFAULT" : "@img")})";
            string cmdInsDescription = @"
                    INSERT INTO descriptions
                    VALUES ((SELECT MAX(id) FROM articles), @en, @de)";

            bool ret = true;

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdInsArticle;
            cmd.Parameters.AddWithValue("name", toBeCreated.Name);
            cmd.Parameters.AddWithValue("price", toBeCreated.Price);
            cmd.Parameters.AddWithValue("category", toBeCreated.Category);
            if (toBeCreated.Image != null) {
                cmd.Parameters.AddWithValue("img", toBeCreated.Image);
            }

            ret &= cmd.ExecuteNonQuery() == 1;

            cmd.CommandText = cmdInsDescription;
            cmd.Parameters.AddWithValue("en", toBeCreated.En);
            cmd.Parameters.AddWithValue("de", toBeCreated.De);

            ret &= cmd.ExecuteNonQuery() == 1;

            return ret;
        }

        public bool UpdateArticle(ArticleAdministration newArticle) {
            string cmdArticleText = @"
                    UPDATE articles
                    SET name  = @name,
                        price = @price,
                        category = @cat,
                        img = @img
                    WHERE id = @id";
            string cmdDescriptionText = @"
                    UPDATE descriptions
                    SET en = @en,
                        de = @de
                    WHERE artId = @id";

            bool ret = true;

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdArticleText;
            cmd.Parameters.AddWithValue("name", newArticle.Name);
            cmd.Parameters.AddWithValue("price", newArticle.Price);
            cmd.Parameters.AddWithValue("cat", newArticle.Category);
            cmd.Parameters.AddWithValue("img", newArticle.Image);
            cmd.Parameters.AddWithValue("id", newArticle.Id);

            ret &= cmd.ExecuteNonQuery() == 1;

            cmd.CommandText = cmdDescriptionText;
            cmd.Parameters.Clear();
            cmd.Parameters.AddWithValue("en", newArticle.En);
            cmd.Parameters.AddWithValue("de", newArticle.De);
            cmd.Parameters.AddWithValue("id", newArticle.Id);

            ret &= cmd.ExecuteNonQuery() == 1;

            return ret;
        }

        public bool AddComment(int artId, Comment comment) {
            string cmdText = @"
                    INSERT INTO comments
                    VALUES (null, @artId, @userId, @comment, @rating)";


            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("artId", artId);
            cmd.Parameters.AddWithValue("userId", comment.User.Id);
            cmd.Parameters.AddWithValue("comment", comment.Text);
            cmd.Parameters.AddWithValue("rating", comment.Rating);

            return cmd.ExecuteNonQuery() == 1;
        }

        public List<Category> GetCategories() {
            string cmdText = $@"
                    SELECT id, title_{LanguageSupport.CurrentLanuageShortString} AS title
                    FROM categories";

            List<Category> cats = new List<Category>();

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                while (reader.Read()) {
                    cats.Add(new Category {
                        Id = Convert.ToInt32(reader["id"]),
                        Title = Convert.ToString(reader["title"])
                    });
                }
            }

            return cats;
        }

        public Category GetCategory(int id) {
            string cmdText = $@"
                    SELECT id, title_{LanguageSupport.CurrentLanuageShortString} AS title
                    FROM categories 
                    WHERE id = @id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("id", id);

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                if (reader.HasRows) {
                    reader.Read();
                    return new Category {
                        Id = Convert.ToInt32(reader["id"]),
                        Title = Convert.ToString(reader["title"])
                    };
                }

                return null;
            }
        }

        public bool OrderArticle(Order order, int? userId, int? addressId) {
            bool isOrderFromUser = userId != null;
            bool ret = true;
            string cmdText = @"
                    INSERT INTO orders
                    VALUES (null, DEFAULT)";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            ret &= cmd.ExecuteNonQuery() == 1;

            if (isOrderFromUser) {
                cmdText = @"
                    INSERT INTO ordersuser
                    VALUES ((SELECT MAX(id) FROM orders), @userId, @addressId)";

                cmd.CommandText = cmdText;
                cmd.Parameters.AddWithValue("userId", userId);
                cmd.Parameters.AddWithValue("addressId", addressId);

                ret &= cmd.ExecuteNonQuery() == 1;
            }
            else {
                OrderNoUser orderNU = (OrderNoUser) order;
                cmdText = @"
                    INSERT INTO ordersnouser
                    VALUES ((SELECT MAX(id) FROM orders), @firstname, @lastname, @email, @street, @housenumber, @postalcode, @city)";
                cmd.CommandText = cmdText;
                cmd.Parameters.AddWithValue("firstname", orderNU.Firstname);
                cmd.Parameters.AddWithValue("lastname", orderNU.Lastname);
                cmd.Parameters.AddWithValue("email", orderNU.Email);
                cmd.Parameters.AddWithValue("street", orderNU.Street);
                cmd.Parameters.AddWithValue("housenumber", orderNU.HouseNumber);
                cmd.Parameters.AddWithValue("postalcode", orderNU.PostalCode);
                cmd.Parameters.AddWithValue("city", orderNU.City);

                ret &= cmd.ExecuteNonQuery() == 1;
            }

            foreach (var orderArticle in order.Articles) {
                cmdText = @"
                        INSERT INTO customerordersarticle
                        VALUES ((SELECT MAX(id) FROM orders), @articleId, @amount)";
                cmd.CommandText = cmdText;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("articleId", orderArticle.Key.Id);
                cmd.Parameters.AddWithValue("amount", orderArticle.Value);

                ret &= cmd.ExecuteNonQuery() == 1;
            }

            return ret;
        }

        public List<ArticleAdministration> GetAllAdministrationArticles() {
            string cmdText = @"
                        SELECT a.id AS artId, a.name, d.en, d.de, a.price, a.img, c.id AS catId
                        FROM articles a
                            INNER JOIN categories c on a.category = c.id
                            INNER JOIN descriptions d on a.id = d.artId";

            List<ArticleAdministration> rets = null;

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                if (reader.HasRows) {
                    rets = new List<ArticleAdministration>();
                    while (reader.Read()) {
                        rets.Add(RowToArticleAdministration(reader));
                    }
                }
            }

            return rets;
        }

        public ArticleAdministration GetArticleAdministration(int id) {
            string cmdText = @"
                        SELECT a.id AS artId, a.name, d.en, d.de, a.price, a.img, c.id AS catId
                        FROM articles a
                            INNER JOIN categories c on a.category = c.id
                            INNER JOIN descriptions d on a.id = d.artId
                        WHERE a.id = @id
                        LIMIT 1";

            ArticleAdministration ret = null;

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("id", id);

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                if (reader.HasRows) {
                    reader.Read();

                    ret = RowToArticleAdministration(reader);
                }
            }

            return ret;
        }

        private Article RowToArticle(MySqlDataReader reader) {
            Article ret = new Article(Convert.ToInt32(reader["id"]), Convert.ToString(reader["name"]),
                Convert.ToString(reader["description"]), Convert.ToDecimal(reader["price"]),
                Convert.ToDouble(reader["rating"] != DBNull.Value ? reader["rating"] : 0.0),
                Convert.ToString(reader["img"]), new List<Comment>());
            return ret;
        }

        private ArticleAdministration RowToArticleAdministration(MySqlDataReader reader) {
            return new ArticleAdministration(Convert.ToInt32(reader["artId"]), Convert.ToString(reader["name"]),
                Convert.ToString(reader["en"]), Convert.ToString(reader["de"]), Convert.ToDecimal(reader["price"]),
                Convert.ToString(reader["img"]), Convert.ToInt32(reader["catId"]));
        }

        private Article AddCommentsToArticle(Article art) {
            if (art == null) {
                return art;
            }

            string cmdGetComments = @"
                    SELECT c.commentText, c.rating, c.userId
                    FROM articles a
                            INNER JOIN comments c on a.id = c.artId
                    WHERE a.id = @id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdGetComments;
            cmd.Parameters.AddWithValue("id", art.Id);

            using (MySqlDataReader readerComments = cmd.ExecuteReader()) {
                if (readerComments.HasRows) {
                    while (readerComments.Read()) {
                        art.Comments.Add(new Comment(Convert.ToString(readerComments["commentText"]),
                            Convert.ToInt32(readerComments["rating"]),
                            new User {Id = Convert.ToInt32(readerComments["userId"])}));
                    }
                }
            }

            using (IUserRepository repo = new UserRepository()) {
                art.Comments.ForEach(x => x.User = repo.GetUser(x.User.Id));
            }

            return art;
        }

        #endregion
    }
}