﻿namespace Shop.Models.Repositories {
    public interface IMysqlConnect {
        void Open();
        void Close();
    }
}
