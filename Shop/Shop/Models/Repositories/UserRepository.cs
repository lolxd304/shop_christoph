﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;

namespace Shop.Models.Repositories {
    public class UserRepository : IUserRepository {
        private string _connectionString = ConfigurationManager.ConnectionStrings["defaultDB"].ConnectionString;
        private MySqlConnection _conn;

        public void Open() {
            if (_conn == null) {
                _conn = new MySqlConnection(_connectionString);
            }

            if (_conn.State == ConnectionState.Open) {
                return;
            }

            _conn.Open();
        }

        public void Close() {
            if (_conn != null && _conn.State == ConnectionState.Open) {
                _conn.Close();
                _conn = null;
            }
        }

        public void Dispose() {
            Close();
        }

        public User AuthenticateUser(Login loginUser) {
            User ret = null;

            string cmdText = @"
                    SELECT *
                    FROM users
                    WHERE (username = @username
                      OR email = @username)
                      AND password = sha2(@password, 256)";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("username", loginUser.Username);
            cmd.Parameters.AddWithValue("password", loginUser.Password);

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                if (reader.HasRows) {
                    reader.Read();
                    ret = new User {
                        Id = Convert.ToInt32(reader["id"]),
                        Email = Convert.ToString(reader["email"]),
                        Firstname = Convert.ToString(reader["firstname"]),
                        Lastname = Convert.ToString(reader["lastname"]),
                        Password = Convert.ToString(reader["password"]),
                        Role = (UserRole) Convert.ToInt32(reader["role"]),
                        Username = Convert.ToString(reader["username"])
                    };
                }
            }

            if (ret != null) {
                ret.Addresses = AddressesToUser(ret.Id);
            }

            return ret;
        }

        public bool InsertUser(User user) {
            string cmdText = $@"
                    INSERT INTO users
                    VALUES (null, @username, sha2(@password, 256), @email, @firstname, @lastname, @role)";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("username", user.Username);
            cmd.Parameters.AddWithValue("password", user.Password);
            cmd.Parameters.AddWithValue("email", user.Email);
            cmd.Parameters.AddWithValue("firstname", user.Firstname);
            cmd.Parameters.AddWithValue("lastname", user.Lastname);
            cmd.Parameters.AddWithValue("role", (int) user.Role);

            return (cmd.ExecuteNonQuery() == 1) && InsertAddressesToUser(user.Addresses, user.Id);
        }

        public bool IsDoubleEmail(string email) {
            string cmdText = @"
                    SELECT email
                    FROM users
                    WHERE email = @email
                    LIMIT 1";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("email", email);

            bool ret;

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                ret = reader.HasRows;
            }

            return ret;
        }

        public bool IsDoubleUsername(string username) {
            string cmdText = @"
                    SELECT username
                    FROM users
                    WHERE username = @username
                    LIMIT 1";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("username", username);

            bool ret;

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                ret = reader.HasRows;
            }

            return ret;
        }

        public bool UpdateUser(User newUser) {
            string cmdText = @"
                    UPDATE users
                    SET username  = @username,
                        password = sha2(@password, 256),
                        email = @email,
                        firstname = @firstname,
                        lastname = @lastname,
                        role = @role
                    WHERE id = @id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("username", newUser.Username);
            cmd.Parameters.AddWithValue("password", newUser.Password);
            cmd.Parameters.AddWithValue("email", newUser.Email);
            cmd.Parameters.AddWithValue("firstname", newUser.Firstname);
            cmd.Parameters.AddWithValue("lastname", newUser.Lastname);
            cmd.Parameters.AddWithValue("role", (int) newUser.Role);
            cmd.Parameters.AddWithValue("id", newUser.Id);

            return cmd.ExecuteNonQuery() == 1;
        }

        public List<Address> AddressesToUser(int userId) {
            string cmdText = @"
                    SELECT *
                    FROM addresses
                    WHERE userId = @uid";

            List<Address> ret = null;

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("uid", userId);

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                if (reader.HasRows) {
                    ret = new List<Address>();
                    while (reader.Read()) {
                        ret.Add(new Address {
                            Id = Convert.ToInt32(reader["id"]),
                            City = Convert.ToString(reader["city"]),
                            HouseNumber = Convert.ToInt32(reader["houseNumber"]),
                            PostalCode = Convert.ToInt32(reader["postalCode"]),
                            Street = Convert.ToString(reader["street"])
                        });
                    }
                }
            }

            return ret;

        }

        public bool InsertAddressesToUser(List<Address> addresses, int userId) {
            string cmdText = @"
                    INSERT INTO addresses
                    VALUES (null, @uid, @street, @houseNumber, @postalCode, @city)";

            bool suc = true;
            MySqlCommand cmd = _conn.CreateCommand();

            foreach (var address in addresses) {
                cmd.CommandText = cmdText;
                cmd.Parameters.Clear();
                cmd.Parameters.AddWithValue("uid", userId);
                cmd.Parameters.AddWithValue("street", address.Street);
                cmd.Parameters.AddWithValue("houseNumber", address.HouseNumber);
                cmd.Parameters.AddWithValue("postalCode", address.PostalCode);
                cmd.Parameters.AddWithValue("city", address.City);

                suc &= cmd.ExecuteNonQuery() == 1;
            }

            return suc;
        }

        public bool DeleteAddress(int id) {
            string cmdText = @"
                    DELETE
                    FROM addresses
                    WHERE id = @id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("id", id);

            return cmd.ExecuteNonQuery() == 1
;
        }

        public bool UpdateAddress(Address newAddress) {
            string cmdText = @"
                    UPDATE addresses
                    SET street      = @street,
                        houseNumber = @houseNumber,
                        postalCode  = @postalCode,
                        city        = @city
                    WHERE id = @id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("street", newAddress.Street);
            cmd.Parameters.AddWithValue("houseNumber", newAddress.HouseNumber);
            cmd.Parameters.AddWithValue("postalCode", newAddress.PostalCode);
            cmd.Parameters.AddWithValue("city", newAddress.City);
            cmd.Parameters.AddWithValue("id", newAddress.Id);

            return cmd.ExecuteNonQuery() == 1;
        }

        public User GetUser(int id) {
            string cmdText = @"
                    SELECT *
                    FROM users
                    WHERE id = @id";

            MySqlCommand cmd = _conn.CreateCommand();
            cmd.CommandText = cmdText;
            cmd.Parameters.AddWithValue("id", id);

            User ret = null;

            using (MySqlDataReader reader = cmd.ExecuteReader()) {
                if (reader.HasRows) {
                    reader.Read();
                    ret = new User
                    {
                        Id = Convert.ToInt32(reader["id"]),
                        Email = Convert.ToString(reader["email"]),
                        Firstname = Convert.ToString(reader["firstname"]),
                        Lastname = Convert.ToString(reader["lastname"]),
                        Password = Convert.ToString(reader["password"]),
                        Role = (UserRole)Convert.ToInt32(reader["role"]),
                        Username = Convert.ToString(reader["username"])
                    };
                }
            }

            if (ret != null) {
                ret.Addresses = AddressesToUser(id);
            }

            return ret;
        }

        public UserRepository() {
            Open();
        }
    }
}