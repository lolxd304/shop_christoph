﻿using System;
using System.Collections.Generic;

namespace Shop.Models.Repositories {
    interface IUserRepository : IMysqlConnect, IDisposable {
        User AuthenticateUser(Login loginUser);
        User GetUser(int id);
        bool InsertUser(User user);
        bool IsDoubleEmail(string email);
        bool IsDoubleUsername(string username);
        bool UpdateUser(User newUser);
        List<Address> AddressesToUser(int userId);
        bool InsertAddressesToUser(List<Address> addresses, int userId);
        bool DeleteAddress(int id);
        bool UpdateAddress(Address newAddress);
    }
}
