﻿function editArticle(id) {
    var popup = window.open("/article/edit/" + id, "Edit article", "menubar=no,location=no,resizable=no,scrollbars=no,status=no,toolbar=no,width=400,height=500");

    var interval = setInterval(function() {
        if (popup.closed) {
            clearInterval(interval);
            location.reload();
        }
    }, 500);
}

function deleteArticle(id) {
    $.ajax({
        url: "/article/delete",
        type: "POST",
        data: {
            id: id
        },
        success: function(result) {
            location.reload();
        }
    });
}

function clickStar(amount) {
    var els = $('#rating_stars i');
    console.log("Triggered!");
    console.log(amount);
    console.log(els);

    for (var i = 0; i < amount; i++) {
        $(els[i]).empty().text('star');
    }
    for (var j = amount; j < 5; j++) {
        $(els[j]).empty().text('star_border');
    }

    $('#Rating').val(amount);
}