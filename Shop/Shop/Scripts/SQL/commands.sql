﻿-- Update articles
UPDATE articles
SET name  = "",
    price = 0.0
WHERE id = 1;

-- Update descriptions
UPDATE descriptions
SET en = "",
    de = ""
WHERE artId = 1;

-- Add comment to article
INSERT INTO comments
VALUES (1, 1, "", 5);

-- Select comments to article
SELECT c.commentText, c.rating
FROM articles a
       INNER JOIN comments c ON a.id = c.artId
WHERE a.id = 1;

-- Order with no user details
INSERT INTO orders
VALUES (null, 1, 20, DEFAULT);

INSERT INTO orderdetailsnouser
VALUES ((SELECT MAX(id) FROM orders), "Firstname", "lastname", "Email", "Street", 12, 126, "City");

-- Get all articles from one category
SELECT a.id, a.name, d.en AS description, a.price, avg(c.rating) AS rating
FROM articles a
       INNER JOIN descriptions d ON a.id = d.artId
       LEFT JOIN comments c ON a.id = c.artId
WHERE a.category = 3
GROUP BY a.id

-- Check for double email
SELECT email
FROM users
WHERE email = "christoph@zalliger.eu"
LIMIT 1;

-- Update user
UPDATE users
SET username  = "Username",
    password  = sha2("Password", 256),
    email     = "Email",
    firstname = "firstname",
    lastname  = "lastname",
    role      = 1
WHERE id = 1;

-- Order article (no user)
INSERT INTO orders
VALUES (null, DEFAULT);

INSERT INTO ordersnouser
VALUES ((SELECT MAX(id) FROM orders), "HH", "hh", "ch@ch.com", "X-Weg", 444, 6789, "x-Stadt");

INSERT INTO customerordersarticle
VALUES ((SELECT MAX(id) FROM orders), 1, 10); -- Repeat for all article