﻿DROP DATABASE IF EXISTS shop_christoph;

CREATE DATABASE shop_christoph CHARACTER SET UTF8 COLLATE utf8_bin;

DROP USER IF EXISTS 'shop_user'@'localhost';
CREATE USER 'shop_user'@'localhost' IDENTIFIED BY 'shop123';
GRANT SELECT, INSERT, UPDATE, DELETE ON shop_christoph.* TO 'shop_user'@'localhost';
FLUSH PRIVILEGES;

USE shop_christoph;

CREATE TABLE users
(
  id        INT AUTO_INCREMENT,
  username  VARCHAR(255) NOT NULL,
  password  VARCHAR(255) NOT NULL,
  email     VARCHAR(255) NOT NULL,
  firstname VARCHAR(255) NOT NULL,
  lastname  VARCHAR(255) NOT NULL,
  role      INT          NOT NULL,
  PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE addresses
(
  id          INT AUTO_INCREMENT,
  userId      INT          NOT NULL,
  street      VARCHAR(100) NOT NULL,
  houseNumber INT          NOT NULL,
  postalCode  INT          NOT NULL,
  city        VARCHAR(100) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT FK_addresses_users FOREIGN KEY (userId) REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE UNIQUE INDEX U_username
  ON users (username);
CREATE UNIQUE INDEX U_email
  ON users (email);

CREATE TABLE categories
(
  id       INT AUTO_INCREMENT,
  title_en TEXT NOT NULL,
  title_de TEXT NOT NULL,
  PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE articles
(
  id       INT AUTO_INCREMENT,
  name     TEXT   NOT NULL,
  price    DOUBLE NOT NULL,
  category INT,
  img      VARCHAR(255) DEFAULT "/content/img/articles/default.jpg",
  PRIMARY KEY (id),
  CONSTRAINT FK_articles_categories FOREIGN KEY (category) REFERENCES categories (id) ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE = InnoDB;

CREATE TABLE descriptions
(
  artId INT,
  en    TEXT,
  de    TEXT,
  PRIMARY KEY (artId),
  CONSTRAINT FK_descriptions_articles FOREIGN KEY (artId) REFERENCES articles (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE comments
(
  id          INT AUTO_INCREMENT,
  artId       INT,
  userId      INT,
  commentText TEXT,
  rating      INT,
  PRIMARY KEY (id),
  CONSTRAINT FK_comments_articles FOREIGN KEY (artId) REFERENCES articles (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT FK_comments_users FOREIGN KEY (userId) REFERENCES users (id) ON UPDATE CASCADE ON DELETE SET NULL
) ENGINE = InnoDB;

CREATE TABLE orders
(
  id        INT AUTO_INCREMENT,
  orderDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE ordersNoUser
(
  orderId     INT AUTO_INCREMENT,
  firstname   VARCHAR(100) NOT NULL,
  lastname    VARCHAR(100) NOT NULL,
  email       VARCHAR(150) NOT NULL,
  street      VARCHAR(100) NOT NULL,
  houseNumber INT          NOT NULL,
  postalCode  INT          NOT NULL,
  city        VARCHAR(100) NOT NULL,
  PRIMARY KEY (orderId),
  CONSTRAINT FK_ordersNoUser_orders FOREIGN KEY (orderId) REFERENCES orders (id) ON UPDATE CASCADE ON DELETE CASCADE

) ENGINE = InnoDB;

CREATE TABLE ordersUser
(
  orderId   INT,
  userId    INT NOT NULL,
  addressId INT NOT NULL,
  PRIMARY KEY (orderId),
  CONSTRAINT FK_ordersUser_orders FOREIGN KEY (orderId) REFERENCES orders (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT FK_ordersUser_users FOREIGN KEY (userId) REFERENCES users (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT FK_ordersUser_addresses FOREIGN KEY (addressId) REFERENCES addresses (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB;

CREATE TABLE customerOrdersArticle
(
  orderId   INT,
  articleId INT,
  amount    INT DEFAULT 0,
  PRIMARY KEY (orderId, articleId),
  CONSTRAINT FK_customerOrderArticle_orders FOREIGN KEY (orderId) REFERENCES orders (id) ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT FK_customerOrderArticle_articles FOREIGN KEY (articleId) REFERENCES articles (id) ON UPDATE CASCADE ON DELETE CASCADE
) ENGINE = InnoDB;