USE shop_christoph;

DELETE
FROM users;

DELETE
FROM articles;

DELETE
FROM categories;

DELETE
FROM descriptions;

DELETE
FROM comments;

DELETE
FROM orders;

DELETE
FROM ordersnouser;

DELETE
FROM ordersuser;

DELETE
FROM customerordersarticle;

DELETE
FROM addresses;

ALTER TABLE articles
  AUTO_INCREMENT = 1;

ALTER TABLE orders
  AUTO_INCREMENT = 1;

ALTER TABLE categories
  AUTO_INCREMENT = 1;

ALTER TABLE comments
  AUTO_INCREMENT = 1;

ALTER TABLE users
  AUTO_INCREMENT = 1;

ALTER TABLE addresses
  AUTO_INCREMENT = 1;

INSERT INTO users
VALUES (null, "Adrian", SHA2("as", 256), "adrian@wannenmacher.eu", "Adrian", "Wannenmacher", 1),
       (null, "Simon", SHA2("as", 256), "simon@raass.com", "Simon", "Raass", 3),
       (null, "Elias", SHA2("as", 256), "elias@rist.com", "Elias", "Rist", 1),
       (null, "Christoph", SHA2("as", 256), "christoph@zallinger.eu", "Christoph", "Zallinger", 3);


INSERT INTO categories
VALUES (null, "Computer & accessories", "Computer & Zubehör"),
       (null, "Sport & freetime", "Sport & Freizeit"),
       (null, "Smartphones", "Smartphones"),
       (null, "Clothing", "Bekleidung");

INSERT INTO articles
VALUES (null, 'OnePlus 6T 128GB', 599.99, 3, "/content/img/articles/oneplus.jpg"),
       (null, 'MSI GeForce RTX 2080 Gaming X Trio', 823.89, 1, "/content/img/articles/msi.jpg"),
       (null, 'AMD Ryzen 2700X 8x3,7GHz', 319.99, 1, "/content/img/articles/amd.jpg"),
       (null, 'Roccat Isku', 78.65, 1, "/content/img/articles/roccat.jpg");

INSERT INTO descriptions
VALUES (1, 'A first class smartphone', 'Ein erstklassiges Smartphone'),
       (2, 'Very nice graphics card!', 'Sehr gute Grafikkarte'),
       (3, 'Top modern CPU', 'Top moderne CPU'),
       (4, 'Very nice and perfect keyboard!', 'Sehr gute und perfekte Tastatur!');

INSERT INTO comments
VALUES (null, 1, 1, "Total super!", 5),
       (null, 1, 2, "Perfect", 5),
       (null, 1, 3, "It's not working", 1),
       (null, 2, 1, "Sehr schnell, aber laut!", 4),
       (null, 3, 1, "Super tolle Rechenleistung", 5),
       (null, 3, 2, "Hat mich überzeugt!", 5);

INSERT INTO addresses
VALUES (null, 1, "Schnittlauchgasse", 32, 6134, "Vomp"),
       (null, 2, "Gaswerkstraße", 4, 6020, "Innsbruck"),
       (null, 3, "Weilerhaus", 22, 6300, "Wörgl"),
       (null, 4, "Feldweg", 485, 6108, "Scharnitz");