﻿function editAddress(id) {
    var popup = window.open("/user/edit/" + id, "Edit article", "menubar=no,location=no,resizable=no,scrollbars=no,status=no,toolbar=no,width=400,height=500");

    var interval = setInterval(function () {
        if (popup.closed) {
            clearInterval(interval);
            location.reload();
        }
    }, 500);
}

function deleteAddress(id) {
    $.ajax({
        url: "/user/deleteaddress",
        type: "POST",
        data: {
            id: id
        },
        success: function (result) {
            location.reload();
        }
    });
}