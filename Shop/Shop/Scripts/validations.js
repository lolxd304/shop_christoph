﻿var errorMessages = new Object();
$.ajax({
    url: "/home/getallvalidationerrors",
    type: "POST",
    success: function (res) {
        for (var key in res) {
            errorMessages[key] = res[key];
        }
        validateRegistration();
    }
});

function validateRegistration() {
    // TODO: add validation for house number and postal code
    $('.form-user-val').validate({
        errorPlacement: function (error, element) {
            element.nextAll('span.helper-text').attr("data-error", error.text());
        },
        errorClass: "invalid",
        validClass: "valid",
        success: function (form) {
            $('span.helper-text').empty();
        },
        rules: {
            Email: {
                required: true,
                email: true
            },
            PasswordReg: {
                minlength: 8,
                required: true,
                pattern: /[\^°!"§$%&/\\\{\}\[\]\(\)=\?´`'#\+\*~\-_\.:,;<>\|ß@€µ]/
            },
            PasswordRepeat: {
                equalTo: "#PasswordReg"
            },
            Username: {
                required: true,
                minlength: 3
            },
            HouseNumber: {
                min: 0,
                required: true
            },
            PostalCode: {
                min: 0,
                required: true
            },
            Street: {
                required: true
            },
            City: {
                required: true
            }
        },
        messages: {
            Email: {
                email: errorMessages["no_valid_email"],
                required: errorMessages["required_email"]
            },
            PasswordReg: {
                required: errorMessages["required_password"],
                minlength: errorMessages["min_pwd"],
                pattern: errorMessages["special_char"]
            },
            PasswordRepeat: {
                equalTo: errorMessages["no_equal_pwds"]
            },
            Username: {
                required: errorMessages["required_username"],
                minlength: errorMessages["min_username"]
            },
            HouseNumber: {
                min: errorMessages["min_house_number"],
                required: errorMessages["required_house_number"]
            },
            PostalCode: {
                min: errorMessages["min_postal_code"],
                required: errorMessages["required_postal_code"]
            },
            Street: {
                required: errorMessages["required_street"]
            },
            City: {
                required: errorMessages["required_city"]
            }
        }
    });
}