﻿var divMessage = $("<div></div>").addClass("col s10");

var iconSuccess = $("<div></div>").addClass("col s2")
    .append($("<i></i>").addClass("material-icons green-text").text("check"));
var iconError = $("<div></div>").addClass("col s2")
    .append($("<i></i>").addClass("material-icons red-text").text("clear"));

var messageLog = '#message_login';
var messageReg = '#message_register';
var messageUpd = '#message_update';

function loginUser() {
    $.ajax({
        url: "/user/login",
        data: {
            Username: $('#Username').val(),
            Password: $('#Password').val()
        },
        type: "POST",
        success: function (result, status, xhr) {
            $(messageLog).empty();
            $(messageReg).empty();

            if (result.success) {
                $(messageLog).append(iconSuccess);
                setTimeout(function() {
                    window.location.href = "/home/index";
                }, 1500);
            } else {
                $(messageLog).append(iconError);
            }

            divMessage.text(result.message.toString());
            $(messageLog).append(divMessage);
        }
    });
}

function registerUser() {
    $.ajax({
        url: "/user/register",
        type: "POST",
        data: getFormData(),
        success: function(result) {
            $(messageReg).empty();
            $(messageLog).empty();

            if (result.success) {
                $(messageReg).append(iconSuccess);
                $.ajax({
                    url: "/login",
                    type: "POST",
                    data: {
                        Username: $('#usernameReg').val(),
                        Password: $('#PasswordReg').val()
                    },
                    success: function(res) {
                        setTimeout(function() {
                            window.location.href = "/";
                        }, 1500);
                    }
                });

            } else {
                $(messageReg).append(iconError);
                for (var key in result.errorMessages) {
                    $('span.helper-text[data-accept_keys]').each(function (index) {
                        if ($(this).data("accept_keys").split(" ").includes(key)) {
                            $(this).prev().addClass("invalid");
                            $(this).attr("data-error", result.errorMessages[key]);
                        }
                    });
                }
            }

            divMessage.text(result.message.toString());
            $(messageReg).append(divMessage);
        }
    });
}

function getFormData() {
    return {
        Firstname: $('#firstname').val(),
        Lastname: $('#lastname').val(),
        Username: $('#usernameReg').val(),
        Password: $('#PasswordReg').val(),
        Email: $('#email').val(),
        Role: 1
    };
}

function updateUser() {
    $.ajax({
        url: "/user/profile",
        type: "POST",
        data: getFormData(),
        success: function(result) {
            if (result.success) {
                $(messageUpd).empty();
                $(messageUpd).append(iconSuccess);
            } else {
                $(messageUpd).append(iconError);
                for (var key in result.errorMessages) {
                    $('span.helper-text[data-accept_keys]').each(function (index) {
                        if ($(this).data("accept_keys").split(" ").includes(key)) {
                            $(this).prev().addClass("invalid");
                            $(this).attr("data-error", result.errorMessages[key]);
                        }
                    });
                }
                if (result.notLoggedIn) {
                    window.location.href = "/login";
                }
            }

            divMessage.text(result.message.toString());
            $(messageUpd).append(divMessage);

            setTimeout(function() {
                $(messageUpd).empty();
            }, 1000);
        }
    });
}