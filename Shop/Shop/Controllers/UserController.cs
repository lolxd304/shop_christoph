﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Management;
using System.Web.Mvc;
using Shop.Models;
using Shop.Models.Repositories;
using WebGrease.Css.Extensions;

namespace Shop.Controllers {
    public class UserController : Controller {
        [HttpGet]
        public ActionResult Login() {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Login userLogin) {
            User curUser = null;

            using (IUserRepository repo = new UserRepository()) {
                curUser = repo.AuthenticateUser(userLogin);
            }

            Session["user"] = curUser;
            Session["basket"] = null;

            return Json(new {
                success = curUser != null, firstname = curUser?.Firstname, lastname = curUser?.Lastname,
                message = LanguageSupport.GetText(curUser == null ? "failed_login" : "successful_login")
            });
        }

        public ActionResult Logout() {
            if ((Session["user"] as User) != null) {
                Session["user"] = null;
                Session["basket"] = null;
                return RedirectToAction("Login");
            }
            else {
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpPost]
        public ActionResult Register(User user) {
            bool suc = false;
            Dictionary<string, string> errors = ValidateUser(user, false);

            if (errors.Count == 0) {
                using (IUserRepository ur = new UserRepository()) {
                    suc = ur.InsertUser(user);
                }

                errors = null;
            }

            return Json(new {
                success = suc,
                message = LanguageSupport.GetText(suc ? "successful_registration" : "failed_registration"),
                errorMessages = errors
            });
        }

        [HttpGet]
        public ActionResult Profile() {
            var user = Session["user"] as User;
            if (user == null) {
                return RedirectToAction("Login");
            }
            else {
                return View(user);
            }
        }

        [HttpPost]
        public ActionResult Profile(User user) {
            bool suc = false;
            var curUser = Session["user"] as User;

            Dictionary<string, string> errors = ValidateUser(user, true);
            if (curUser == null) {
                return Json(new {success = suc, notLoggedIn = true});
            }
            else {
                if (errors.Count == 0) {
                    user.Id = curUser.Id;
                    user.Role = curUser.Role;
                    using (IUserRepository repo = new UserRepository()) {
                        suc = repo.UpdateUser(user);
                    }

                    if (suc) {
                        Session["user"] = user;
                    }
                }
            }

            return Json(new {
                success = suc,
                message = LanguageSupport.GetText(suc ? "update_successful" : "update_error"),
                errorMessages = errors
            });
        }

        public ActionResult Addresses() {
            if (!(Session["user"] is User u)) {
                return RedirectToAction("Login");
            }

            return View(u.Addresses);
        }

        public ActionResult Edit(int? id) {
            if (!(Session["user"] is User u)) {
                return RedirectToAction("Login");
            }

            Address a = null;
            if (id != null) {
                a = u.Addresses.First(x => x.Id == id);
            }

            return View(a);
        }

        [HttpPost]
        public ActionResult Edit(Address add) {
            if (!(Session["user"] is User u))
            {
                return RedirectToAction("Login");
            }

            bool suc = false;

            if (add != null) {
                using (IUserRepository repo = new UserRepository()) {
                    if (add.Id != Int32.MinValue) {
                        suc = repo.UpdateAddress(add);
                    }
                    else {
                        suc = repo.InsertAddressesToUser(new List<Address> {add}, u.Id);
                    }

                    u.Addresses = repo.AddressesToUser(u.Id);
                }
            }

            ViewBag.Close = suc;

            return View(add);
        }

        public JsonResult DeleteAddress(int id) {
            if (!(Session["user"] is User u)) {
                return Json(new { success = false });
            }

            bool suc;

            using (IUserRepository repo = new UserRepository()) {
                suc = repo.DeleteAddress(id);
                u.Addresses = repo.AddressesToUser(u.Id);
            }

            return Json(new {success = suc});
        }

        private Dictionary<string, string> ValidateUser(User toValidate, bool isUpdateStatement) {
            string key;
            Dictionary<string, string> errors = new Dictionary<string, string>();

            if (string.IsNullOrEmpty(toValidate.Email)) {
                key = "required_email";
                errors.Add(key, LanguageSupport.GetText(key));
            }

            if (!toValidate.Email.Contains("@")) {
                key = "no_valid_email";
                errors.Add(key, LanguageSupport.GetText(key));
            }

            using (IUserRepository repo = new UserRepository()) {
                if (!isUpdateStatement) {
                    if (repo.IsDoubleEmail(toValidate.Email)) {
                        key = "double_email";
                        errors.Add(key, LanguageSupport.GetText(key));
                    }

                    if (repo.IsDoubleUsername(toValidate.Username)) {
                        key = "double_username";
                        errors.Add(key, LanguageSupport.GetText(key));
                    }
                }
                else {
                    if (((User) Session["user"])?.Email != toValidate.Email && repo.IsDoubleEmail(toValidate.Email)) {
                        key = "double_email";
                        errors.Add(key, LanguageSupport.GetText(key));
                    }

                    if (((User) Session["user"])?.Username != toValidate.Username && repo.IsDoubleUsername(toValidate.Username)) {
                        key = "double_username";
                        errors.Add(key, LanguageSupport.GetText(key));
                    }
                }
            }

            if (string.IsNullOrEmpty(toValidate.Password)) {
                key = "required_password";
                errors.Add(key, LanguageSupport.GetText(key));
            }

            if (toValidate.Password.Length < 8) {
                key = "min_pwd";
                errors.Add(key, LanguageSupport.GetText(key));
            }

            if (!Regex.IsMatch(toValidate.Password, @"[\^°!""§$% &/\\\{\}\[\]\(\)=\?´`'#\+\*~\-_\.:,;<>\|ß@€µ]")) {
                key = "special_char";
                errors.Add(key, LanguageSupport.GetText(key));
            }

            if (string.IsNullOrEmpty(toValidate.Username)) {
                key = "required_username";
                errors.Add(key, LanguageSupport.GetText(key));
            }

            if (toValidate.Username.Length < 3) {
                key = "min_username";
                errors.Add(key, LanguageSupport.GetText(key));
            }

            return errors;
        }
    }
}