﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Shop.Models;
using Shop.Models.Repositories;
using WebGrease.Css.Extensions;

namespace Shop.Controllers {
    public class HomeController : Controller {
        public ActionResult Index(string q, int? cat) {
            IArticleRepository artRepo = new ArticleRepository();
            List<Article> articles = new List<Article>();

            if (!string.IsNullOrEmpty(q) && (cat != null)) {
                articles = cat != -1 ? artRepo.SearchArticles(q, (int)cat) : artRepo.SearchArticles(q);
            }
            else if (string.IsNullOrEmpty(q) && (cat!=null)) {
                articles = cat != -1 ? artRepo.GetAllArticlesFromCategory((int) cat) : artRepo.GetAllArticles();
            }
            else if (!string.IsNullOrEmpty(q) && (cat == null)) {
                articles = artRepo.SearchArticles(q);
            }
            else {
                articles = artRepo.GetAllArticles();
            }

            ViewBag.Cats = artRepo.GetCategories();
            ViewBag.CurrentCat = artRepo.GetCategory(cat ?? int.MaxValue);

            artRepo.Dispose();

            return View(articles);
        }

        public ActionResult SetLanguage(string lang) {
            switch (lang) {
                case "de":
                    LanguageSupport.CurrentLanguage = Lang.de;
                    break;
                default:
                    LanguageSupport.CurrentLanguage = Lang.en;
                    break;
            }

            if (Request.UrlReferrer == null) {
                return RedirectToAction("Index", "Home");
            }

            return Redirect(Request.UrlReferrer.ToString());
        }

        [HttpPost]
        public JsonResult GetAllValidationErrors() {
            string[] keys = {
                "required_email",
                "no_valid_email",
                "required_password",
                "min_pwd",
                "no_equal_pwds",
                "required_username",
                "min_username",
                "special_char",
                "required_postal_code",
                "required_city",
                "required_house_number",
                "required_street",
                "min_house_number",
                "min_postal_code"
            };
            Dictionary<string, string> texts = new Dictionary<string, string>();

            keys.ForEach(k => texts.Add(k, LanguageSupport.GetText(k)));

            return Json(texts);
        }
    }
}