﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Shop.Models;
using Shop.Models.Repositories;

namespace Shop.Controllers {
    public class ArticleController : Controller {
        // GET: Article
        public ActionResult Index(int? id) {
            if (id == null) {
                return RedirectToAction("Index", "Home");
            }
            Article art;
            using (IArticleRepository repo = new ArticleRepository()) {
                art = repo.GetArticle((int)id);
            }

            if (art == null) {
                return RedirectToAction("Index", "Home");
            }

            return View(art);
        }

        public ActionResult Basket() {
            return View((Dictionary<Article, int>)Session["basket"]);
        }

        [HttpPost]
        public ActionResult Basket(int articleId, int amount) {
            if (Session["basket"] == null) {
                Session["basket"] = new Dictionary<Article, int>();
            }
            Article art = null;
            Dictionary<Article, int> basket = Session["basket"] as Dictionary<Article, int>;
            using (IArticleRepository repo = new ArticleRepository()) {
                art = repo.GetArticle(articleId);
            }

            if (art != null) {
                if (!basket.Select(x => x.Key.Id).ToList().Contains(art.Id)) {
                    basket?.Add(art, amount);
                }
                else {
                    basket[basket.First(x => x.Key.Id == art.Id).Key] += amount;
                }
            }

            return View(basket);
        }

        public ActionResult Order() {
            if (Session["basket"] == null) {
                return RedirectToAction("Index", "Home");
            }

            Order ord;

            if (Session["user"] == null) {
                ord = new Order {
                    Articles = Session["basket"] as Dictionary<Article, int>,
                    Id = -1,
                    OrderDate = DateTime.Now
                };
            }
            else {
                ord = new OrderNoUser {
                    Articles = Session["basket"] as Dictionary<Article, int>,
                    Id = -1,
                    OrderDate = DateTime.Now
                };
            }

            return View(ord);
        }

        [HttpPost]
        public ActionResult Order(OrderNoUser order, int? group_address) {
            if (Session["basket"] == null) {
                return RedirectToAction("Index", "Home");
            }

            bool suc = false;

            order.Articles = Session["basket"] as Dictionary<Article, int>;
            using (IArticleRepository artRepo = new ArticleRepository()) {
                suc = artRepo.OrderArticle(order, (Session["user"] as User)?.Id, group_address);
            }

            if (suc) {
                ClearBasket();
                return View("Message", new Message
                {
                    Headline = LanguageSupport.GetText("order_success"),
                    Msg = LanguageSupport.GetText("order_success_long")
                });
            }
            else {
                return View("Message", new Message {
                    Headline = LanguageSupport.GetText("order_error"),
                    Solution = LanguageSupport.GetText("try_later")
                });
            }
        }

        public ActionResult ClearShoppingBasket() {
            ClearBasket();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Management() {
            if (!(Session["user"] is User)) {
                return RedirectToAction("Login", "User");
            }

            List<ArticleAdministration> articles = null;

            using (IArticleRepository repo = new ArticleRepository()) {
                articles = repo.GetAllAdministrationArticles();
            }

            if (articles == null) {
                return View("Message", new Message {
                    Headline = LanguageSupport.GetText("not_found"),
                    Solution = LanguageSupport.GetText("try_later")
                });
            }
            else {
                return View(articles);
            }
        }

        public ActionResult Edit(int? id) {
            if (!(Session["user"] is User)) {
                return RedirectToAction("Login", "User");
            }

            ArticleAdministration art = null;

            if (id != null) {
                using (IArticleRepository repo = new ArticleRepository()) {
                    art = repo.GetArticleAdministration((int)id);
                    ViewBag.Categories = repo.GetCategories();
                }
            }
            else {
                using (IArticleRepository repo = new ArticleRepository()) {
                    ViewBag.Categories = repo.GetCategories();
                }
            }
            return View(art);
        }

        [HttpPost]
        public ActionResult Edit(ArticleAdministration art, HttpPostedFileBase newImg) {
            bool suc = true;

            if (art != null) {
                if (newImg != null && newImg.ContentLength > 0 &&
                    new[] {"image/jpeg", "image/jpg", "image/png"}.Contains(newImg.ContentType)) {
                    string path = Path.Combine(Server.MapPath("~/Content"), "img/articles/");
                    string virtualPath = "/content/img/articles/" + newImg.FileName;
                    art.Image = virtualPath;
                    newImg.SaveAs(path + newImg.FileName);
                }

                using (IArticleRepository artRepo = new ArticleRepository()) {
                    if (art.Id != -1) {
                        suc &= artRepo.UpdateArticle(art);
                    }
                    else {
                        suc &= artRepo.CreateArticle(art);
                    }
                }
            }
            else {
                return View("Message", new Message {
                    Headline = "Error"
                });
            }

            using (IArticleRepository repo = new ArticleRepository()) {
                ViewBag.Categories = repo.GetCategories();
            }

            ViewBag.Close = suc;
            return View();
        }

        [HttpPost]
        public JsonResult Delete(int id) {
            if (!(Session["user"] is User)) {
                return Json(new {success = false});
            }

            bool suc;

            using (IArticleRepository repo = new ArticleRepository()) {
                suc = repo.DeleteArticle(id);
            }

            return Json(new {success = suc});
        }

        [HttpPost]
        public ActionResult Comment(Comment c, int artId) {
            if (!(Session["user"] is User u)) {
                return RedirectToAction("Login", "user");
            }

            if (c != null) {
                c.User = u;
                using (IArticleRepository repo = new ArticleRepository()) {
                    repo.AddComment(artId, c);
                }
            }

            return RedirectToAction("Index", new {id = artId});
        }

        private void ClearBasket() {
            Session["basket"] = null;
        }
    }
}